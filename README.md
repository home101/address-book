# AddressBook

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.3.2.

## Development server
Start Java Spring `addressbook` backend to provide web api for this project.
This demo is hardcoded to use with `addressbook` application web api for url: `http://localhost:8080`

- Run `npm install` in project's directory (cmd/terminal) 
- Run `npm install -g @angular/cli` in project's directory (cmd/terminal)
- Then `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Requirements
- node.js
- require java back-end `addressbook` project
