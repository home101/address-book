import { Component, OnInit } from '@angular/core';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/distinctUntilChanged';
import { ActivatedRoute, Router } from '@angular/router';
import { AddressBookService } from '../shared/address-book.service';
import { AddressBook } from '../models/address-book';

@Component({
    selector: 'address-book-form',
    templateUrl: './address-book-form.component.html',
    styleUrls: ['./address-book-form.component.css']
})
export class AddressBookFormComponent implements OnInit
{
    addressBook: AddressBook;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _addressBookService: AddressBookService
    )
    {
        _route.params.subscribe(p =>
        {
            if (p["id"] != undefined)
                this._addressBookService
                    .getAddressBook(p["id"])
                    .subscribe(addressBook => this.addressBook = addressBook);
        })
    }

    ngOnInit()
    {
    }

    add(name)
    {
        this._addressBookService
            .addAddressBook(name)
            .subscribe(x =>
            {
                this._router.navigate(['']);
            })
    }

    save()
    {
        this._addressBookService
            .saveAddressBook(this.addressBook)
            .subscribe(x =>
            {
                this._router.navigate(['']);
            })
    }

    delete()
    {
        this._addressBookService
            .deleteAddressBook(this.addressBook.id)
            .subscribe(x =>
            {
                this._router.navigate(['']);
            })
    }
}
