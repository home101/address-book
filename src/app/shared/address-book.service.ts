import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Helper } from './helper';

@Injectable()
export class AddressBookService
{

    private url = "http://localhost:8080/api";

    constructor(private _http: Http) { }

    getAddressBooks()
    {
        return this._http
            .get(this.url + "/address-books/")
            .map(res =>
            {
                let jsonObject = res.json();
                let addressbooks = Helper.toArrayObjects(jsonObject);
                addressbooks.forEach(addressBook =>
                {
                    addressBook["contacts"] = Helper.toArrayObjects(addressBook["contacts"]);
                });
                return addressbooks;
            });
    }

    getAddressBook(id)
    {
        return this._http
            .get(this.url + "/address-books/" + id)
            .map(res =>
            {
                let addressBook = res.json();
                let contacts = Helper.toArrayObjects(addressBook["contacts"]);
                addressBook["contacts"] = contacts;
                return addressBook;
            });
    }

    addAddressBook(name)
    {
        let form = new FormData();
        form.append("name", name);

        return this._http
            .post(this.url + "/address-books/add", form)
            .map(res =>
            {
                let addressBook = res.json();
                let contacts = Helper.toArrayObjects(addressBook["contacts"]);
                addressBook["contacts"] = contacts;
                return addressBook;
            });
    }

    saveAddressBook(addressBook)
    {
        return this._http
            .put(this.url + "/address-books/update", Helper.toJavaHashMap(addressBook))
            .map(res =>
            {
                let addressBook = res.json();
                let contacts = Helper.toArrayObjects(addressBook["contacts"]);
                addressBook["contacts"] = contacts;
                return addressBook;
            });
    }


    deleteAddressBook(id)
    {
        return this._http
            .delete(this.url + "/address-books/delete/" + id)
            .map(res => res.json());
    }

}