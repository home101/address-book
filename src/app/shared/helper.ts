import { AddressBook } from "../models/address-book";

export class Helper
{
    static toArrayObjects(obj)
    {
        let tmp = new Array<any>();
        let keys = Object.keys(obj);
        keys.forEach(key =>
        {
            tmp.push(obj[key]);
        });
        return tmp;
    }

    static toJavaHashMap(addressBook: AddressBook)
    {
        let hmAddressBook = {
            id: addressBook.id,
            name: addressBook.name,
            contacts: {}
        };

        let newContacts = {};
        addressBook.contacts.forEach(contact =>
        {
            newContacts[contact.id] = contact;
        });

        hmAddressBook.contacts = newContacts;

        return hmAddressBook;
    }
}