import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class ContactService
{
    private url = "http://localhost:8080/api/address-books/";

    constructor(private _http: Http) { }

    getContacts(addressBookId, contactId)
    {
        return this._http
            .get(this.url + addressBookId + "/contacts/" + contactId)
            .map(res => res.json());
    }

    addContact(addressBookId, name, phone)
    {
        let form = new FormData();
        form.append("name", name);
        form.append("phone", phone);

        return this._http
            .post(this.url + addressBookId + "/contacts/add", form)
            .map(res => res.json());
    }

    saveContact(addressBookId, contact)
    {
        return this._http
            .put(this.url + addressBookId + "/contacts/update", contact)
            .map(res => res.json());
    }

    deleteContact(addressBookId, contactId)
    {
        return this._http
            .delete(this.url + addressBookId + "/contacts/delete/" + contactId)
            .map(res => res.json());
    }


}