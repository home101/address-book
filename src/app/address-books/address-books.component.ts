import { Component, OnInit } from '@angular/core';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/distinctUntilChanged';
import { AddressBookService } from '../shared/address-book.service';
import { AddressBook } from '../models/address-book';
import { asEnumerable } from 'linq-es2015';
import { Contact } from '../models/contact';
import { Router } from '@angular/router';

@Component({
    selector: 'address-books',
    templateUrl: './address-books.component.html',
    styleUrls: ['./address-books.component.css']
})

export class AddressBooksComponent implements OnInit
{
    addressBooks: AddressBook[];
    contacts = [];
    selectedAddressBookId;
    uniqueContacts = [];
    disabled = true;

    constructor(
        private _addressBookService: AddressBookService,
        private _router: Router
    ) { }

    ngOnInit()
    {
        this._addressBookService
            .getAddressBooks()
            .subscribe(addressBooks => this.addressBooks = addressBooks);

    }

    showUniqueContacts()
    {
        let contactsArr = asEnumerable(this.addressBooks)
            .Select(a => a.contacts)
            .ToArray();

        let contacts = new Array<Contact>();
        for (let i = 0; i < contactsArr.length; i++)
        {
            contactsArr[i].forEach(contact =>
            {
                contacts.push(contact);
            });
        }

        this.uniqueContacts = asEnumerable(contacts)
            .Distinct(c => c.name)
            .ToArray();

        console.log(this.uniqueContacts);

    }

    close()
    {
        this.uniqueContacts = [];
    }

    onEdit(cId)
    {
        this._router.navigate(['/contact/edit/', this.selectedAddressBookId, cId]);
    }

    addContact()
    {
        this._router.navigate(['/contact/new', this.selectedAddressBookId]);
    }

    selected(id)
    {
        if (id !== "null")
        {
            this.selectedAddressBookId = id;
            this.disabled = false;
            this.contacts = asEnumerable(this.addressBooks)
                .FirstOrDefault(addressBook => addressBook.id === id).contacts;
        } else
        {
            this.disabled = true;
            this.contacts = [];
        }
    }
}
