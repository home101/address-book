import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AddressBookFormComponent } from './address-book-form/address-book-form.component';
import { AddressBooksComponent } from './address-books/address-books.component';
import { ContactsFormComponent } from './contacts-form/contacts-form.component';
import { AddressBookService } from './shared/address-book.service';
import { ContactService } from './shared/contact.service';

@NgModule({
    declarations: [
        AppComponent,
        AddressBookFormComponent,
        AddressBooksComponent,
        ContactsFormComponent
    ],
    imports: [
        HttpModule,
        BrowserModule,
        FormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'address-books', pathMatch: 'full' },
            { path: 'address-books', component: AddressBooksComponent },
            { path: 'address-book/new', component: AddressBookFormComponent },
            { path: 'address-book/:id', component: AddressBookFormComponent },
            { path: 'contact/edit/:abId/:cId', component: ContactsFormComponent },
            { path: 'contact/new/:abId', component: ContactsFormComponent }
        ], { useHash: true })
    ],
    providers: [
        AddressBookService,
        ContactService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
