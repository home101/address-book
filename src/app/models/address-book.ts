import { Contact } from "./contact";

export class AddressBook
{
    id: number;
    name: string;
    contacts: Contact[];
}