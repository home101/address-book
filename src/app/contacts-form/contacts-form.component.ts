import { Component, OnInit } from '@angular/core';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/distinctUntilChanged';
import { ActivatedRoute, Router } from '@angular/router';
import { ContactService } from '../shared/contact.service';
import { Contact } from '../models/contact';

@Component({
    selector: 'contacts-form',
    templateUrl: './contacts-form.component.html',
    styleUrls: ['./contacts-form.component.css']
})
export class ContactsFormComponent implements OnInit
{

    contact: Contact;
    addressBookId;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _contactService: ContactService
    )
    {
        _route.params.subscribe(ids =>
        {
            this.addressBookId = ids['abId'];
            if (ids['cId'] != undefined)
                this._contactService
                    .getContacts(this.addressBookId, ids['cId'])
                    .subscribe(contact => this.contact = contact);
        });
    }

    ngOnInit()
    {
    }

    add(name, phone)
    {
        this._contactService.addContact(this.addressBookId, name, phone)
            .subscribe(c =>
            {
                this._router.navigate(['']);
            })
    }

    save()
    {
        this._contactService
            .saveContact(this.addressBookId, this.contact)
            .subscribe(c =>
            {
                this._router.navigate(['']);
            });
    }

    delete()
    {
        if (confirm("Are you sure?"))
        {
            this._contactService
                .deleteContact(this.addressBookId, this.contact.id)
                .subscribe(x =>
                {
                    this._router.navigate(['']);
                })
        }
    }
}
